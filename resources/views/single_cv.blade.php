<?php
use App\cv_database;
?>
@extends ('layouts.base')

@section ('header')
 {{ $cv->name }}
@endsection

@section ('content')

<div class="row">

			<div class="col-md-3">

<table  class="table  table-hover">
<tr>
<th>El. paštas: </th>
<td>{{ $cv->email }}</td>
</tr>
<tr>
<th>Darbo sritis: </th>
<td><?=$cv->cvtable()->first()->name ?></td>
</tr>
<tr>
<th>Patalpinimo data</th>
<td>{{ $cv->created_at }}</td>
</tr>

</table>

</div>

</div>
@endsection