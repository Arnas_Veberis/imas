@extends ('layouts.base')

@section ('header')

CV įkėlimo forma

@endsection

@section ('content')


<form action="{{ url('/new_cv') }}" method="post" enctype="multipart/form-data">
<div class="form-group row">
  <label for="example-text-input" class="col-xs-2 col-form-label">Vardas</label>
  <div class="col-xs-3">
    <input class="form-control" type="text"  name="name" value="{{ Request::old('name') }}">
  </div>
</div>
<div class="form-group row">
  <label for="example-search-input" class="col-xs-2 col-form-label">El. paštas</label>
  <div class="col-xs-3">
    <input class="form-control" type="email"  name="email" value="{{ Request::old('email') }}">
  </div>
</div>
<div class="form-group row">
  <label for="example-email-input" class="col-xs-2 col-form-label">Darbo sritis</label>
  <div class="col-xs-3">
    <select class="form-control" name="job_field" >
    @foreach ($jobs as $j)
    <option value="<?=$j->id?>" <?=(Request::old('job_field')==$j->id)?"selected='selected'":""?> ><?=$j->name?></option>
    @endforeach
    </select>
  </div>
</div>

<div class="form-group row">
  <label for="example-email-input" class="col-xs-2 col-form-label">Prisekite savo cv (doc, docx, pdf)</label>
  <div class="col-xs-3">
    <input class="form-control" type="file" name="failas" value="{{ Request::old('failas') }}">
  </div>
</div>


<?php echo csrf_field();?>
<div class="form-group row">
<label for="example-email-input" class="col-xs-2 col-form-label"></label>
  <div class="col-xs-3">
    <input class="form-control" type="submit"  name="action">
  </div>
</div>

</form>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@endsection