@extends ('layouts.base')


@section ('header')

Turimų gyvenimo aprašymų sąrašas

@endsection


@section ('content')

<div class="container" id="content" tabindex="-1">


			

		<div class="row">

			<div class="col-md-6">
			
				<table  class="table  table-hover">
					<thead>
					<tr>
    <th>Data</th>
    <th>Darbo sritis</th>
    <th></th>
    
  </tr>
					</thead>

  				@foreach ($cv as $c)
 <tr>
    <td><?=$c->created_at?></td>
    <td><?=$c->cvtable()->first()->name?></td>
    <td><a href="{{ url('/cv')}}/<?=$c->id?>">Plačiau</a></td>
    
   </tr>	
    @endforeach


				</table>

			</div>
			
			<div class="col-md-6">
<form action="{{ url('/new_cv') }}" method="get"><input type="submit" value="Pridėkite savo CV!!"></form>
</div>
			
		</div>
	</div>

@endsection