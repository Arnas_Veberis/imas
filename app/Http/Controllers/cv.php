<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cv_database as cvdb;
use Illuminate\Support\Facades\DB;
use App\job as jobdb;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;


class cv extends Controller
{
	/**
	 * Paimami visi CV ir perduodami index.blade.php atvaizdavimui pagrindiniame puslapyje
	 * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
	 */
	
    public function home(){
    	$data['cv']=cvdb::orderBy('created_at', 'desc')->get();
    	
    	return view('index', $data);
    }
    
    /**
     * Pagrindiniame tinklapyje pasirinkus 'placiau' paduodams cv id, paimami duomenys is DB ir paduodami atvaizdavimui
     * @integer cv $id
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    
    public function single_cv($id){
    	$data['cv']=cvdb::find($id);
    	return view('single_cv', $data);
    }
    
    /**
     * Pridedant nauja cv paimami darbo poziciju variantai select'ui formoje
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    
    public function new_cv(){
    	$data['jobs']=jobdb::all();
    	return view('new_cv', $data);    	
    }
	
  /**
   * Patikrinami formos duomenys
   * Prisegtas cv patalpinamas storage/app/public, sulinkintas su public/storage, todel pasiekiams per sugeneruota asset url
   * Issiunciami formos duomenys kartu su cv url i DB
   * @param Request $request
   * @return \Illuminate\Http\RedirectResponse
   */
    
    public function add_cv(Request $request){
    	
    	$this->validate($request, ['name' => 'required|max:40',
    							  'email' => 'required|unique:cv_databases,email',
    							  'failas' => 'required|file|mimes:doc,docx,pdf'],
    									['name.required' => 'Įveskite vardą',
    									'name.max' => 'Per ilgas vardas',
    									'email.required' => 'Įveskite savo el. paštą',
    									'email.unique' => 'Toks vartotojas jau egzistuoja',
    									'failas.required' => 'Prisekite CV',
    									'failas.mimes' => 'Neleidžiams failo tipas']);
    	
    	$ext='.'.$request->file('failas')->getClientOriginalExtension();
    	$file=$request->file('failas')->storeAs('', $request->email.$ext); // Kadangi, emailas yra unikalus, toks ir failo pavadinimas
    	
    	
    	$cv=new cvdb();
    	$cv->name=$request->name;
    	$cv->email=$request->email;
    	$cv->job_field=$request->job_field;
    	$cv->cv_url=asset(Storage::url($file));
    	$cv->save();    	
    	
    	return redirect()->route('index');
    }
}
