<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'cv@home')->name('index');

Auth::routes();

Route::get('cv/{id}', 'cv@single_cv');

Route::get('/new_cv', 'cv@new_cv');

Route::post('/new_cv', 'cv@add_cv');

Route::get('/home', 'HomeController@index');
